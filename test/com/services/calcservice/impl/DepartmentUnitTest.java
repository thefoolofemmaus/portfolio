package com.services.calcservice.impl;

import java.math.BigDecimal;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import com.services.calcservice.impl.Department;
import com.services.calcservice.impl.Developer;
import com.services.calcservice.impl.Manager;
import com.services.calcservice.impl.QATester;

public class DepartmentUnitTest {
	
	public Department testDepartment;
	
	@Test
	public void testConstructor() {
		testDepartment = new Department();
		Assert.assertNotNull(testDepartment);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetAllocationError() {
		testDepartment = new Department();
		testDepartment.getAllocation();
	}
	
	@Test
	public void testGetAllocation_Manager() {
		testDepartment = new Department();
		testDepartment.add(new Manager());
		Assert.assertThat(testDepartment.getAllocation(), Is.is(BigDecimal.valueOf(300)));
	}
	
	
	@Test
	public void testGetAllocation_Changes() {
		testDepartment = new Department();
		Manager man1 = new Manager();
		Developer dev1 = new Developer();
		QATester qat1 = new QATester();
		testDepartment.add(man1);
		Assert.assertThat(testDepartment.getAllocation(), Is.is(BigDecimal.valueOf(300)));
		testDepartment.add(dev1);
		Assert.assertThat(testDepartment.getAllocation(), Is.is(BigDecimal.valueOf(1300)));
		testDepartment.add(qat1);
		Assert.assertThat(testDepartment.getAllocation(), Is.is(BigDecimal.valueOf(1800)));
		testDepartment.remove(dev1);
		Assert.assertThat(testDepartment.getAllocation(), Is.is(BigDecimal.valueOf(800)));
		
		
	}
}
