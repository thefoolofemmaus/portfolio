package com.services.calcservice.impl;

import java.math.BigDecimal;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import com.services.calcservice.impl.QATester;

public class QATesterUnitTest {
	@Test
	public void testConstructor() {
		QATester qat1 = new QATester();
		Assert.assertNotNull(qat1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveEmployeeError() {
		
		QATester qat1 = new QATester();
		QATester qat2 = new QATester();
		qat1.remove(qat2);
	}
	
	@Test
	public void testAllocation() {
		QATester qat1 = new QATester();
		Assert.assertThat(qat1.getAllocation(), Is.is(BigDecimal.valueOf(500)));
	}
}
