package com.services.calcservice.impl;

import java.math.BigDecimal;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

public class ManagerUnitTest {

	@Test
	public void testConstructor() {
		Manager man1 = new Manager();
		Assert.assertNotNull(man1);
		Assert.assertThat(man1.getAllocation(), Is.is(BigDecimal.valueOf(300)));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveError() {
		Manager man1 = new Manager();
		Developer dev1 = new Developer();
		man1.remove(dev1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveWrongEmployeeError() {
		Manager man1 = new Manager();
		Developer dev1 = new Developer();
		QATester qat1 = new QATester();
		
		man1.add(dev1);
		man1.remove(qat1);
	}
	
	@Test
	public void testGetAllocation(){
		Manager man1 = new Manager();
		Developer dev1 = new Developer();
		QATester qat1 = new QATester();
		
		Assert.assertThat(man1.getAllocation(), Is.is(BigDecimal.valueOf(300)));
		
		man1.add(dev1);
		Assert.assertThat(man1.getAllocation(), Is.is(BigDecimal.valueOf(1300)));
	}

}
