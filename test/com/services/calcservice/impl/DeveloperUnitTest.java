package com.services.calcservice.impl;

import java.math.BigDecimal;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import com.services.calcservice.impl.Developer;

public class DeveloperUnitTest {
	
	@Test
	public void testConstructor() {
		Developer dev1 = new Developer();
		Assert.assertNotNull(dev1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveEmployeeError() {
		
		Developer dev1 = new Developer();
		Developer dev2 = new Developer();
		dev1.remove(dev2);
	}
	
	@Test
	public void testAllocation() {
		Developer dev1 = new Developer();
		Assert.assertThat(dev1.getAllocation(), Is.is(BigDecimal.valueOf(1000)));
	}
}
