package com.services.calcservice.utils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {

	Properties prop = new Properties();
	String propFileName = "config.properties";	
	InputStream inputStream;
	
	public PropertyLoader() {
		try {
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("Error: Property file '" + propFileName + "' not found in the classpath");
			}
		} catch(Exception e) {
			System.out.println("Exception: " + e);
		}
	}
	
	public String getProperty(String keyName) {
		return prop.getProperty(keyName);
	}
}
