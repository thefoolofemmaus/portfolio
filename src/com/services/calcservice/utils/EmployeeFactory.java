package com.services.calcservice.utils;

import com.services.calcservice.Employee;
import com.services.calcservice.impl.Department;
import com.services.calcservice.impl.Developer;
import com.services.calcservice.impl.Manager;

public class EmployeeFactory {
	
	private static String department;
	private static String manager;
	private static String developer;
	private static String qatester;
	private static String invalidArgumentError;
	private static String nullArgumentError;
	
	public EmployeeFactory() {
		PropertyLoader props = new PropertyLoader();
		department=props.getProperty("Department");
		manager=props.getProperty("Manager");
		developer=props.getProperty("Developer");
		qatester=props.getProperty("QATester");
		invalidArgumentError = props.getProperty("InvalidArgumentError");
		nullArgumentError = props.getProperty("NullArgumentError");
	}
	
	public Employee getEmployee(String employeeType) {
		
		Employee returnEmployee;
		
		if (null == employeeType) {
			throw new IllegalArgumentException(nullArgumentError);
		} else if (department == employeeType) {
			returnEmployee = new Department();
		} else if (manager == employeeType) {
			returnEmployee = new Manager();
		} else if (developer == employeeType) {
			returnEmployee = new Developer();
		} else if (qatester == employeeType) {
			returnEmployee = new Developer();
		} else {
			throw new IllegalArgumentException(invalidArgumentError);
		}
		
		return returnEmployee;
	}
}
