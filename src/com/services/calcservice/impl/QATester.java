package com.services.calcservice.impl;

import java.math.BigDecimal;

import com.services.calcservice.Employee;
import com.services.calcservice.utils.PropertyLoader;

public class QATester implements Employee {
	
	private static BigDecimal allocation;
	
	private static String addError;
	private static String removeError;
	
	
	public QATester() {
		
		PropertyLoader props = new PropertyLoader();
		
		allocation = new BigDecimal(props.getProperty("QATesterAllocation"));
		addError = props.getProperty("QATesterAddError");
		removeError = props.getProperty("QATesterRemoveError");
		
	}
	
	@Override
	public BigDecimal getAllocation() {
		return QATester.allocation;
	}
	
	@Override
	public void add(Employee employee)  {
		throw new IllegalArgumentException(addError);
	}
	
	@Override
	public void remove(Employee employee) {
		throw new IllegalArgumentException(removeError);	
	}
}
