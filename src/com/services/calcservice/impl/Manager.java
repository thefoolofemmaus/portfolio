package com.services.calcservice.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.services.calcservice.Employee;
import com.services.calcservice.utils.PropertyLoader;

public class Manager implements Employee {
	
	private static BigDecimal allocation;
	private List<Employee> employees = new ArrayList<Employee>();
	private static String removeError;
	private static String removeExistsError;
	
	public Manager() {
		
		PropertyLoader props = new PropertyLoader();
		
		allocation = new BigDecimal(props.getProperty("ManagerAllocation"));
		removeError = props.getProperty("ManagerRemoveError");
		removeExistsError = props.getProperty("ManagerRemoveExistsError");
		
	}
	

	@Override
	public BigDecimal getAllocation() {
		
		BigDecimal totalAllocation = allocation;
		
		for(Employee e: employees) {
			totalAllocation = totalAllocation.add(e.getAllocation());
		}
		
		return totalAllocation;
	}

	@Override
	public void add(Employee employee) {
		employees.add(employee);
	}

	@Override
	public void remove(Employee employee) {
		if (employees.isEmpty()) {
			throw new IllegalArgumentException(removeError);
		} else if (!employees.contains(employee)) {
			throw new IllegalArgumentException(removeExistsError);
		} else {
			employees.remove(employee);
		}
	}
}
