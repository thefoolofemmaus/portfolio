package com.services.calcservice.impl;

import java.math.BigDecimal;

import com.services.calcservice.Employee;
import com.services.calcservice.utils.PropertyLoader;

public class Developer implements Employee {

	private static BigDecimal allocation;
	private static String addError;
	private static String removeError;
	
	public Developer() {
		
		PropertyLoader props = new PropertyLoader();
		
		allocation = new BigDecimal(props.getProperty("DeveloperAllocation"));
		addError = props.getProperty("DeveloperAddError");
		removeError = props.getProperty("DeveloperRemoveError");
		
	}
	
	@Override
	public BigDecimal getAllocation() {
		return Developer.allocation;
	}

	@Override
	public void add(Employee employee)  {
		throw new IllegalArgumentException(addError);
	}
	
	@Override
	public void remove(Employee employee) {
		throw new IllegalArgumentException(removeError);
	}
}
