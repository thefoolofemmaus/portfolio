package com.services.calcservice.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.services.calcservice.Employee;
import com.services.calcservice.utils.PropertyLoader;

//TODO: this class is identical to Manager, less the configuration. Find an elegant way to combine them.

public class Department implements Employee {
	
	private static BigDecimal allocation;
	private List<Employee> employees = new ArrayList<Employee>();
	private static String removeError;
	private static String noEmployeeError;
	private static String removeExistsError;
	
	public Department() {
		
		PropertyLoader props = new PropertyLoader();
		
		removeError = props.getProperty("DepartmentRemoveError");
		noEmployeeError = props.getProperty("DepartmentNoEmployeeError");
		removeExistsError = props.getProperty("DepartmentRemoveExistsError");
	}
	

	@Override
	public BigDecimal getAllocation() {
		
		BigDecimal totalAllocation = BigDecimal.ZERO;
		if (employees.isEmpty()) {
			throw new IllegalArgumentException(noEmployeeError);
		} else {
			for(Employee e: employees) {
				totalAllocation = totalAllocation.add(e.getAllocation());
			}
		}
		
		return totalAllocation;
	}

	@Override
	public void add(Employee employee) {
		employees.add(employee);
	}

	@Override
	public void remove(Employee employee) {
		if (employees.isEmpty()) {
			throw new IllegalArgumentException(removeError);
		} else if (!employees.contains(employee)) {
			throw new IllegalArgumentException(removeExistsError);
		} else {
			employees.remove(employee);
		}
	}
}
